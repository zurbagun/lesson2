<?php
/**
Домашнее задание к уроку №2
 */
//Объявляем массивы
$zoo = ['Africa'=>['Ursus arctos crowtheri','Tachyoryctes splendens','Tragelaphus'],
    'Eurasia'=>['Rhinoceros binagadensis','Acinonyx jubatus venaticus','Elephas'],
    'Nord America'=>['Procyon lotor','Ovibos moschatus','Cervus elaphus subspp'],
    'South America'=>['Callicebus melanochir','Microsciurus mimulus','Dolichotis'],
    'Antarctica'=>['Hydrurga leptonyx','Eubalaena australis','Lagenorhynchus'],
    'Australia'=>['Pseudonaja','Moloch horridus','Sarcophilus harrisii']
];
//Объявляем пустой массив
$ZooTwoWorlds = [];

//Логика
//Перебираем массив
foreach ($zoo as $value)
    //Перебираем вложенный массив
    for ($x = 0;$x < count($value); $x++)
        //Проверяем количество слов в предложении
        if (str_word_count(trim($value[$x]),0) == 2)
            {
               //Добавляем в новый массив
                array_push($ZooTwoWorlds,$value[$x]);
                //echo "<b> $value[$x]</b><br>";
              // print_r($ZooTwoWorlds);
            }

//Перемешиваем массив
// Формируем массив с первыми словами названий животных
$ZooFirstWorlds = [];
// И массив со вторыми словами
$ZooNextWorlds = [];
//Пробегаемся по массиву
foreach ($ZooTwoWorlds as $key)
    {
    //Берем первое слово из предложения  и Добавляем в новый массив
        $world =  explode(' ',$key);
        array_push($ZooFirstWorlds,"$world[0] ");
        array_push($ZooNextWorlds, $world[1]);
    }
//Перемешиваем слова во втором массиве
shuffle($ZooNextWorlds);
//Соединяем оба массива
$NewZoo = [];
for ($z = 0; $z < count($ZooFirstWorlds); $z++){

    array_push($NewZoo,$ZooFirstWorlds[$z] . $ZooNextWorlds[$z]);
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Lesson2</title>
</head>
<body>
<!--Вывод переменных-->
<ol>
    <b><li>Исходный массив</li></b>
        <p><?php var_dump($zoo)?></p>
    <b><li>Названия, состоящие из двух слов:</li></b>
        <p><?php echo implode(", ",$ZooTwoWorlds);?></p>
    <b><li>"Фантазийные" названия:</li></b>
       <p> <?php
        foreach ($NewZoo as $key)
            echo "<p>$key</p>";
        ?>
        </p>

</ol>
</body>
</html>